import axios from '../../../configs/api.service'
import { openRegisterSuccessNotification } from './registerNotification'

const registerService = (form) => {
  return {
    register: async user => {
      const { data } = await axios.post('/users', user)
      const { _id, username } = data
      openRegisterSuccessNotification(`User ${username} register successfully. And get ID: ${_id}`)
    },
  }
}

export default registerService
