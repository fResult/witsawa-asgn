import React, { lazy, Suspense } from 'react'
import { Switch, Route, useRouteMatch, useLocation } from 'react-router-dom'

const Register = lazy(() => import('./Register'))

const routes = [
  { path: '/', Component: Register },
]

const Routes = () => {
  const match = useRouteMatch()
  const location = useLocation()
  return (
    <Suspense fallback={<div />}>
      <Switch location={location}>
        {routes.map(({ path, Component }) => (
          <Route key={path} exact={true} path={`${match.path}${path}`} component={Component} />
        ))}
      </Switch>
    </Suspense>
  )
}

export default Routes
