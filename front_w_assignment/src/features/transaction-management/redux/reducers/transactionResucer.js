import { transactionTypes } from '../types'

const initTransactionState = {
  type: '',
  amount: 0,
  remark: '',
  date: new Date(),
}

const transactionReducer = (state = initTransactionState, action) => {
  switch (action.type) {
    case transactionTypes.SELECT_TRANSACTION:
    default:
      return state
  }
}

export default transactionReducer
