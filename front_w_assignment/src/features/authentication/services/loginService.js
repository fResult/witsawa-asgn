import { openLoginSuccessNotification } from './loginNotification'

const loginService = () => {
  return {
    login: async (username, password) => {
      const userInfo = { _id: '5e5971a36f64dd001421f8c6', username: 'Korn1' }
      // const { data } = await axios.post('/users/login', { username, password })
      // openLoginSuccessNotification(`Username ${username} login successfully`)
      openLoginSuccessNotification(`Username ${userInfo.username} login successfully`)
      // localStorage.setItem('userInfo', JSON.stringify({ _id: data._id, username }))
      localStorage.setItem('userInfo', JSON.stringify(userInfo)) // ล็อกอินจริง แล้วติด CORS Blocked
      // return { _id: data._id, username }
      return { _id: userInfo._id, username }
    },
  }
}

export default loginService
