import React from 'react'
import { notification } from 'antd'
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons'

export const openLoginSuccessNotification = (message) => {
  notification.success({
    icon: <CheckCircleOutlined style={{ fontSize: 40, color: '#52C41A' }} />,
    message: 'Login Successfully',
    description: message,
    placement: 'bottomRight',
    duration: 7,
    style: {
      width: 600,
      marginLeft: 335 - 600,
    },
  })
}

export const openLoginFailedNotification = (message) => {
  notification.error({
    icon: <CloseCircleOutlined style={{ fontSize: 40, color: '#FF4D4F' }} />,
    message: 'Login Failed',
    description: message,
    placement: 'bottomRight',
    duration: 4.5,
    style: {
      width: 600,
      marginLeft: 335 - 600,
    },
  })
}
