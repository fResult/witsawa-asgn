import { authenticationTypes } from '../types'

export const loginSuccess = () => {
  return {
    type: authenticationTypes.LOGIN_SUCCESS,
    isAuthenticated: true,
    userInfo: JSON.parse(localStorage.getItem('userInfo'))
  }
}

export const logoutSuccess = () => {
  return {
    type: authenticationTypes.LOGOUT_SUCCESS,
    isAuthenticated: false,
  }
}
