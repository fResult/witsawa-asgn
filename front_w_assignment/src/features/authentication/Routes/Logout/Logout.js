import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { connect } from 'react-redux'
import { logoutSuccess } from '../../redux/actions/authenticationAction'
import { LoadingOutlined } from '@ant-design/icons'

const Logout = ({ logoutSuccess }) => {

  const history = useHistory()

  useEffect(() => {
    handleLogout()
  }, [handleLogout])

  function handleLogout() {
    localStorage.removeItem('userInfo')
    setTimeout(() => history.push('/auth/login'), 3000)
    logoutSuccess()
  }

  return (
    <>
      <div style={{ fontSize: 128, position: 'fixed', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}>
        <LoadingOutlined />
      </div>
    </>
  )
}

export default connect(
  null,
  { logoutSuccess },
)(Logout)
