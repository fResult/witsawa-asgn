import axios from "axios";

axios.defaults.baseURL = 'https://assignment-api.dev.witsawa.com';

export default axios;
