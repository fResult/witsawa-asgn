import React from 'react'
import { Col, Modal, Row, Typography } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'

const { Text } = Typography

const ModalConfirm = ({ onConfirmRemove, visible, transactionId }) => {
  return (
    <Modal
      visible={visible}
      title="Are you sure ?"
      onOk={() => onConfirmRemove(true, transactionId)}
      onCancel={async () => await onConfirmRemove(false, transactionId)}
      okText="Confirm"
      okType="danger"
      bodyStyle={{ background: '#5787BF' }}
    >
      <Row>
        <Col span={18}>
          <Text type="warning" style={{ fontSize: 24 }}>Do you want to delete this transaction ?</Text>
        </Col>
        <Col span={6} style={{textAlign: 'center'}}>
          <ExclamationCircleOutlined style={{ fontSize: 60, color: '#FAAD14' }} />
        </Col>
      </Row>
    </Modal>
  )
  // Modal.confirm({
  //   title: 'Do you want to delete these items?',
  //   icon: <ExclamationCircleOutlined />,
  //   content: 'When clicked the OK button, this dialog will be closed after 1 second',
  //   onOk: () => onConfirmRemove(true, transactionId),
  //   onCancel: () => onConfirmRemove(false, transactionId),
  // })
}

export default ModalConfirm
