import React from 'react'
import { Redirect, Route } from 'react-router-dom'

const PrivateRoute = ({ component: Component, ...rest }) => {
  // const isAuthenticated = useSelector(state => state.authenticationReducer.isAuthenticated)
  const userInfo = JSON.parse(localStorage.getItem('userInfo'))
  const isAuthenticated = !!userInfo

  return (
    <Route {...rest} render={routeProps => {
      return isAuthenticated ? (
        <Component exact {...routeProps} />
      ) : (
        <Redirect to={{ pathname: '/auth/login', state: { from: routeProps.location } }} />
      )
    }} />
  )
}

export default PrivateRoute
