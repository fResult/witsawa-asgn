import React from 'react'
import { Button, DatePicker, Form, Input, Modal, Select } from 'antd'
import { AlignLeftOutlined, CalendarOutlined, DeleteOutlined, DollarCircleOutlined } from '@ant-design/icons'
import moment from 'moment'
import ModalConfirm from '../ModalConfirm'

const ModalTransactionForm = props => {
  const {
    transaction, modalConfirmVisible, visible, removeLoading, onCancel, onAddOrEditTransaction, onRemoveTransaction,
    onConfirmRemoveTransaction,
  } = props

  const [form] = Form.useForm()
  const { amount, date, remark, type } = transaction


  return (
    <>
      <Modal visible={visible}
             title={<h3 style={{ textAlign: 'center' }}>{transaction._id ? 'Edit' : 'Add'} Transaction</h3>}
             okText={transaction._id ? 'Edit' : 'Add'}
             cancelText="Cancel"
             onCancel={onCancel}
             onOk={() => {
               form
                 .validateFields()
                 .then(values => {
                   form.resetFields()
                   onAddOrEditTransaction(values)
                 })
                 .catch(info => console.warn('Validate Failed:', info))
             }}
      >
        <Form
          form={form}
          layout="vertical"
          name="editTransaction"
          size='large'
          initialValues={{ type, amount, remark, date: moment(date) }}
        >
          <Form.Item name="type"
                     rules={[
                       { required: true, message: 'Please select Type' },
                     ]}>
            <Select placeholder="Type">
              <Select.Option value="income">Income</Select.Option>
              <Select.Option value="expense">Expense</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="amount"
            rules={[
              () => ({
                validator(rule, value) {
                  if (/^[0-9]+\.?[0-9]*$/.test(value) && '' + parseInt(value) >= 1)
                    return Promise.resolve()
                  else if (value === '')
                    return Promise.reject('Please input Amount')
                  else if (typeof value !== 'number')
                    return Promise.reject('Amount must be only a number')
                  return Promise.reject('Amount must be 1 or more')
                },
              }),
            ]}
          >
            <Input addonBefore={<DollarCircleOutlined />} placeholder="Amount" />
          </Form.Item>
          <Form.Item name="remark"
                     rules={[
                       { required: true, message: 'Please input Note' },
                     ]}>
            <Input addonBefore={<AlignLeftOutlined />} placeholder="Note" />
          </Form.Item>
          <Form.Item name="date" rules={[
            { required: true, message: 'Please select Date' },
          ]}>
            <DatePicker addonBefore={<CalendarOutlined />} style={{ width: '100%' }} />
          </Form.Item>
        </Form>
        {transaction._id && (
          <div style={{ textAlign: 'right' }}>
            <Button type="danger" loading={removeLoading} onClick={() => {
              onRemoveTransaction(transaction._id)
            }}>
              Remove<DeleteOutlined />
            </Button>
          </div>
        )}
      </Modal>

      {modalConfirmVisible && (
        <ModalConfirm onConfirmRemove={onConfirmRemoveTransaction} visible={modalConfirmVisible}
                      transactionId={transaction._id}
        />
      )}
    </>
  )
}

export default ModalTransactionForm
