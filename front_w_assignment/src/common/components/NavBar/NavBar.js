import React from 'react'
import { Button } from 'antd'
import { useHistory } from 'react-router-dom'
import { LoadingOutlined } from '@ant-design/icons'

const NavBar = ({ logoutLoading }) => {

  const history = useHistory();
  const userInfo = localStorage.getItem('userInfo')
  // const userInfo = useSelector(state => state.authenticationReducer.userInfo)

  return (
    <>
      <div align="right">
        {!userInfo ? (
          <Button onClick={() => history.push('/auth/login')}>Login</Button>
        ) : (
          <>
            {userInfo.username && <Button>{userInfo.username}</Button>}
            <Button onClick={() => history.push('/auth/logout')} disabled={logoutLoading}>
              {!logoutLoading ? 'Logout' : <LoadingOutlined />}
            </Button>
          </>
        )}
      </div>
    </>
  )
}

export default NavBar
