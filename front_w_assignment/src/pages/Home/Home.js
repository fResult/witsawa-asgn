import React, { useState } from 'react'
import { Layout } from 'antd'
import NavBar from '../../common/components/NavBar'
import styles from './Home.module.css'
import Routes from '../../App/Routes'
import Logout from '../../features/authentication/Routes/Logout'

const { Header, Content, Footer } = Layout

const Home = () => {

  const [logoutLoading, setLogoutLoading] = useState(false)

  const handleChangeLogoutLoading = isLoading => {
    setLogoutLoading(isLoading)
  }

  return (
    <>
      <Layout className={styles.Layout} style={{ zIndex: 0, height: '100vh' }}>
        <Header className={styles.Header}>
          <NavBar logoutLoading={logoutLoading} />
        </Header>
        <Content className={styles.Content} style={{ padding: '0' }}>
          <div style={{ padding: 80, minHeight: 760 }}>
            <Routes />
          </div>
        </Content>
        <Footer className={styles.Footer}>fResult ©2020</Footer>
      </Layout>

      {/*<Logout logoutLoading={logoutLoading} onChangeLogoutLoading={handleChangeLogoutLoading} style={{ display: 'none' }} />*/}
    </>
  )
}

export default Home
