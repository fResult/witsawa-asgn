import React, { useEffect, useState } from 'react'
import moment from 'moment'
import axios from '../../configs/api.service'
import { Avatar, Button, Card, Col, List, Row, Skeleton } from 'antd'
import { EllipsisOutlined, PlusOutlined } from '@ant-design/icons'
import ModalTransactionForm from '../../common/components/ModalTransactionForm'
import styles from './Daily.module.css'
import { Decimal } from 'decimal.js'

function Daily() {
  const [userInfo] = useState(JSON.parse(localStorage.getItem('userInfo')))
  const [transactions, setTransactions] = useState([])
  const [transaction, setTransaction] = useState({ type: '', date: new Date(), remark: '', amount: 0 })

  const [fetchLoading, setFetchLoading] = useState(false)
  const [editLoading, setEditLoading] = useState(false)
  const [removeLoading, setRemoveLoading] = useState(false)

  const [modalTransactionVisible, setModalTransactionVisible] = useState(false)
  const [modalConfirmVisible, setModalConfirmVisible] = useState(false)

  useEffect(() => {
    setFetchLoading(true);
    (async () => {
      try {
        setTransactions(await fetchTransactions())
      } catch (err) {
        console.error(err)
      }
    })()
    setFetchLoading(false)
  }, [modalConfirmVisible, fetchLoading])

  const groups = transactions.reduce((transactions, transaction) => {
    const [date] = transaction.date.split('T')
    if (!transactions[date]) transactions[date] = []
    transactions[date].push(transaction)
    return transactions
  }, [])

  const transactionGroupsByDate = Object.keys(groups).map((date) => {
    return { date, transactions: groups[date] }
  })

  async function fetchTransactions() {
    const result = userInfo && await axios.get(`/transactions?user=${userInfo._id}`)
    return result.data.sort((a, b) => new Date(b.date) - new Date(a.date))
  }

  function formatDate(dateTime, format) {
    return moment(dateTime).format(format)
  }

  function handleSelectTransaction(id) {
    setTransaction(transactions.filter(transaction => transaction._id === id)[0])
    setModalTransactionVisible(true)
  }

  function handleOpenAddTransaction() {
    setTransaction({ amount: 0 })
    setModalTransactionVisible(true)
  }

  async function handleAddOrEditTransaction(values) {
    setEditLoading(true)

    if (transaction._id) {
      await axios.put(`/transactions/${transaction._id}`, {
        _id: transaction._id,
        date: values.date,
        remark: values.remark,
        amount: values.amount,
        type: values.type.toLowerCase(),
      })
    } else {
      await axios.post(`/transactions`, {
        user: userInfo._id,
        date: values.date,
        remark: values.remark,
        amount: values.amount,
        type: values.type,
      })
    }

    setEditLoading(false)
    setModalTransactionVisible(false)
    setTransactions(await fetchTransactions())
  }

  async function handleRemoveTransaction(id) {
    setRemoveLoading(true)
    setModalConfirmVisible(() => true)
  }

  async function handleConfirmRemoveTransaction(isConfirm, id) {
    await setModalConfirmVisible(false)
    if (isConfirm) {
      await axios.delete(`/transactions/${id}`)
      setTransactions(await fetchTransactions())
      setModalTransactionVisible(false)
    }
    setRemoveLoading(false)
    setModalConfirmVisible(false)
  }

  return (
    <>
      <div align="center">
        <h1 style={{ position: 'fixed', top: 75, left: '50%', transform: 'translate(-50%)', zIndex: 1 }}>
          Daily Expense and Income
        </h1>
        <ul>
          {transactionGroupsByDate.map(groupTransactions => (
            <Card className={styles.Card}
                  key={groupTransactions.date}
                  bodyStyle={{ padding: '0 10px' }}
                  title={
                    <Row>
                      <Col span={5} style={{ textAlign: 'left' }}>
                        <Row>
                          <Col style={{ fontSize: 36 }}>
                            {formatDate(groupTransactions.date, 'DD')}
                          </Col>
                          <Col>
                            <Row>
                              {formatDate(groupTransactions.date, 'dddd')}
                            </Row>
                            <Row>
                              {formatDate(groupTransactions.date, 'MMM YYYY')}
                            </Row>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={19} style={{ fontSize: 18, textAlign: 'right' }}>
                        {'฿ ' + groupTransactions.transactions.reduce((prev, transaction) => {
                          prev = new Decimal(prev)
                          const amount = new Decimal(transaction.amount)
                          return transaction.type === 'income' ? prev.plus(amount) : prev.minus(amount)
                        }, 0)}
                      </Col>
                    </Row>
                  }
            >
              <List
                size="large"
                dataSource={groupTransactions.transactions}
                renderItem={transaction => (
                  <List.Item key={transaction._id}
                             className={styles.EachTransaction}
                             onClick={() => handleSelectTransaction(transaction._id)}
                  >
                    <Skeleton loading={fetchLoading} active avatar>
                      <List.Item.Meta
                        avatar={<Avatar icon={<EllipsisOutlined />} />}
                        description={
                          <Row className={styles.ListItemMetaDesc}>
                            <Col span={16} style={{ textAlign: 'left' }}>{transaction.remark}</Col>
                            <Col span={8} style={{
                              textAlign: 'right',
                              color: transaction.type === 'expense' ? 'crimson' : 'dodgerblue',
                            }}>
                              {`฿ ${transaction.amount}`}
                            </Col>
                          </Row>
                        }
                      />
                    </Skeleton>
                  </List.Item>
                )}
              />
            </Card>
          ))}
        </ul>

        <div className={styles.ContainerButtonAdd}>
          <Button type="primary" className={styles.ButtonAdd} onClick={() => handleOpenAddTransaction()}>
            <PlusOutlined />
          </Button>
        </div>
      </div>

      {modalTransactionVisible && (
        <ModalTransactionForm
          onCancel={() => setModalTransactionVisible(false)} onRemoveTransaction={handleRemoveTransaction}
          onAddOrEditTransaction={handleAddOrEditTransaction} removeLoading={removeLoading}
          transaction={transaction}
          visible={true} editLoading={editLoading} onConfirmRemoveTransaction={handleConfirmRemoveTransaction}
          modalConfirmVisible={modalConfirmVisible}
        />
      )}
    </>
  )
}

export default Daily
