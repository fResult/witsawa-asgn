Expense and Income Management
==
📖 Description
--
It is a Frontend project that I do follow by Witsawa corp's assignment  
Frontend: ReactJS with React hooks  
CSS Framework: Ant Design V.4.0.0

💪 Features
--
* Signup and Sign in
* Display transactions order by date and calculate daily amount
* Create new transaction (Expense or Income)
* Edit transaction
* Remove transaction


😀 Author:
--
Sila Setthakan-anan (fResult)
